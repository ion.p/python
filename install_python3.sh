#!/bin/bash
wget https://www.python.org/ftp/python/3.4.4/Python-3.4.4.tar.xz
tar xf Python-3.4.4.tar.xz
cd Python-3.4.4
./configure --prefix=/opt/python3
make && make install
alias python='/opt/python3/bin/python3.4'
. ~/.bashrc
